package zad1;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class GUI extends JFrame {
	public void perform() {
		
		JFrame window = setFrame();
		JPanel panel = setPanel(window);
		JLabel label = setLabel(panel);
		setButton(panel, label);	 	
	}
	
	JPanel setPanel(JFrame window) {
		JPanel panel = new JPanel(new BorderLayout());
		window.add(panel);
		return panel;
	}
	
	JLabel setLabel(JPanel c) {
		JLabel label = new JLabel("", SwingConstants.CENTER);
		c.add(label, BorderLayout.CENTER);
		return label;	
	}
	
	void setButton(JPanel c, JLabel label) {
		
		JButton button = new JButton("Wynik");
		c.add(button, BorderLayout.SOUTH);
		
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				label.setText(Main.calculate(Fibonacci.arr));
				
			}
		});
		
	}
	
	JFrame setFrame() {
		JFrame window = new JFrame("Zadanie 1");
		window.setLocationRelativeTo(null);
		window.setVisible(true);
		window.setSize(300,250);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		return window;
	}

} 
